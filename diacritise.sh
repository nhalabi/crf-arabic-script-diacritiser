#!/bin/bash

# You might need this for CRF++ depending on the platform (probably not needed for windows).
# Please change the assinment as suitable by your directory structure.
# Lib is the directory containing library which CRF++ is depending on.
export LD_LIBRARY_PATH=${BASEDIR}/lib/

# Run diacritisation pipeline.
python preprocess.py 2 <&0 | crf_test -m models/latest_model | python postprocess.py >&1