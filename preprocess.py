import sys
import re
import codecs

diac_patt = u'([\u03a6\u064e\u064b\u064f\u064c\u0650\u064d\u0652\u0651]+)'
not_diac_patt = u'([^\u03a6\u064e\u064b\u064f\u064c\u0650\u064d\u0652\u0651\r\n])'
undiac_patt = u'([^\r\n\u03a6\u064e\u064b\u064f\u064c\u0650\u064d\u0652\u0651])'
patt = undiac_patt + diac_patt
replace_patt = u'\\1\t\\2\n'
split_patt = u'[\(\){},.;\'"\:*\u060c\u061f\u061b\u0021\u00bb\u00ab\s]'
punc_merge_patt = u'([\(\){},.;\'"\:*\u060c\u061f\u061b\u0021\u00bb\u00ab])'

def get_word_char_map(words, span):
	"""Return a dictionary which maps for every character index, a word context
	(list of words) to which it belongs to in addition to the character's index
	in the word. These are used as features for CRF++ sometimes.
	"""
	
	wc_map = {}
	cur = 0
	contexts = [[u'\u03a6'] * i + words[:-i] for i in range(span, 0, -1)] + \
			   [words[i:] + [u'\u03a6'] * i for i in range(0, span + 1)]
	
	for context in zip(*contexts):
		word = context[span]
		
		affix_context = []
		for i in range(-1, 2):
			if len(context[span + i]) > 3:
				affix_context.append(context[span + i][:3])
				affix_context.append(context[span + i][:2])
				affix_context.append(context[span + i][-3:])
				affix_context.append(context[span + i][-2:])
			else:
				affix_context.append(u'\u03a6')
				affix_context.append(u'\u03a6')
				affix_context.append(u'\u03a6')
				affix_context.append(u'\u03a6')
		
		for i in range(len(word) + 1):
			wc_map[i + cur] = tuple(affix_context) + context + (str(i),)	
		
		cur += len(word) + 1
		
	return wc_map

def preprocess(text, word_span):
	"""Preprocess text for vocalisation using CRF++.
	"""
	
	## Add empty diac characters (equivalent to no diac in theory)
	## And perform some cleaning before processing
	text = re.sub(u'\r\n', u'\n', text)
	text = re.sub(u'[ \\t\\f\\v]+', u' ', text)
	text = re.sub(u' ' + punc_merge_patt, u'\\1', text)
	text = re.sub(punc_merge_patt + u' ', u'\\1', text)
	text = re.sub(not_diac_patt + not_diac_patt, u'\\1\u03a6\\2', text)
	text = re.sub(not_diac_patt + not_diac_patt, u'\\1\u03a6\\2', text)
	text = re.sub(not_diac_patt + u'([\r\n])', u'\\1\u03a6\\2', text)
	text = re.sub(not_diac_patt + u'$', u'\\1\u03a6', text)
	
	lines = text.splitlines()
	
	res = []
	for line in lines:
		clean_line = line.strip()
		
		if clean_line != u'':
			clean_line = u'.\u03a6' + clean_line + u'.\u03a6'
			## Get character-to-word/s maps.
			undiac_line = re.sub(diac_patt, u'', clean_line)
			words = re.split(split_patt, undiac_line)
			wc_map = get_word_char_map(words, word_span)
			
			## Split letter/diacs each into a line.
			clean_line = re.sub(patt, replace_patt, clean_line)
			
			## Add the word context features to each line.
			bits = clean_line.split(u'\n')
			bits_done = []
			for i, bit in enumerate(bits[:-1]):
				bit_line = u'\t'.join(wc_map[i]) + u'\t' + bit
				bits_done.append(bit_line)
			
			bits_done.append(u'')			
			clean_line = u'\n'.join(bits_done)
		
			## Sub spaces as they confuse CRF and other ML systems.
			clean_line = re.sub(u' ', u'<s>', clean_line)
			
			res.append(clean_line)
	
	res = u'\n'.join(res)
	res = re.sub(u'\n\n\n', u'\n\n', res)
	## Clean empty characters
	res = re.sub(u'\t([\r\n\t])', u'\t\u03a6\\1', res)
	res = re.sub(u'([\r\n\t])\t', u'\\1\u03a6\t', res)
	
	return res
	
def main():
	word_span = int(sys.argv[1])
	
	sys.stdin = codecs.getreader('utf-8')(sys.stdin.buffer, 'strict')
	text = sys.stdin.read()
		
	preprocessed_text = preprocess(text, word_span)
	sys.stdout = codecs.getwriter('utf-8')(sys.stdout.buffer, 'strict')
	print(preprocessed_text)

if __name__ == '__main__':
	main()