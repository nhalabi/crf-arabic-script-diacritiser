import sys
import re
import codecs

if __name__ == '__main__':
	sys.stdin = codecs.getreader('utf-8')(sys.stdin.buffer, 'strict')
	sys.stdout = codecs.getwriter('utf-8')(sys.stdout.buffer, 'strict')
	
	for line in sys.stdin:
		clean_line = re.sub(r'<s>', u' ', line.strip())
		clean_line = re.sub(u'\u03a6', u'', clean_line)
		parts = clean_line.split(u'\t')[-3::2]
		
		bit = u''.join(parts)
		sys.stdout.write(bit)
		if bit == u'':
			sys.stdout.write(u'\n')