import zipfile
from lxml import etree
import glob
import codecs
import re
def get_epub_info(zip, fname):
	ns = {
		'n': 'urn:oasis:names:tc:opendocument:xmlns:container',
		'pkg': 'http://www.idpf.org/2007/opf',
		'dc': 'http://purl.org/dc/elements/1.1/'
	}
	
	# find the contents metafile
	txt = zip.read(fname)
	try:
		tree = etree.fromstring(txt)
		notags = etree.tostring(tree, encoding='utf8', method='text')
		return notags
	except etree.XMLSyntaxError as er:
		print('error')
		print(fname)
		
	return ''
	
res = []
for epub in glob.glob('epub/*'):
	zip = zipfile.ZipFile(epub)
	files = [f for f in zip.namelist() if len(f.split('/')) > 1 and f.split('/')[-2].lower() == 'content']
	
	for file in files:
		text = get_epub_info(zip, file)
		text = text.decode('utf-8')
		lines = text.splitlines()
		
		for line in lines:
			clean_line = re.sub(u'[\s]+', u' ', line).strip()
			count = len(re.findall(u'[\u03a6\u064e\u064b\u064f\u064c\u0650\u064d\u0652\u0651]', clean_line))
			
			if len(clean_line) > 0 and count / float(len(clean_line)) > 0.3:
				res.append(clean_line)

for txt in glob.glob('text/*'):
	with codecs.open(txt, 'r', encoding='utf-8') as file:
		text = file.read()
		
		lines = text.splitlines()
		
		for line in lines:
			clean_line = re.sub(u'[\s]+', u' ', line).strip()
			count = len(re.findall(u'[\u03a6\u064e\u064b\u064f\u064c\u0650\u064d\u0652\u0651]', clean_line))
			
			if len(clean_line) > 0 and count / float(len(clean_line)) > 0.35:
				res.append(clean_line)
				
with codecs.open('res', 'w', encoding='utf-8') as file:
	file.write(u'\n'.join(res))