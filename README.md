# CRF Arabic Diacritiser by Nawar Halabi (nawar.halabi@gmail.com)

This is Nawar Halabi's CRF based diacritiser for Arabic, optimised using several experiments using millions of features.

## Dependencies

This diacritiser can run on any platform (including mobile phones).

* Python 3.6.* or above preferred.
* CRF++ installed and crf_test command available in the PATH variable.

## Installation

* Install python 3.6 or above (preferably).
* Install CRF++ (see instructions on their website for installing on your specific platfrom).

## Usage

An example shell script is available which shows how to run the diacritiser on linux. The shell script can easily be ported to suit the platform an the purpose (web or mobile etc...).

If you wish to avoid using Python, you can use Pyinstaller to compile python files to fully independant binaries to your specific platform. You will need to do this for preprocess.py and postprocess.py, as CRF++ has a separate compilation processes which you can find online (Windows binaries are available).